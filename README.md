# Mars Robot

## API

Two end-points are available. The first one send a movement instruction to the Robot. The second one, reset the position of the Robot. When reseted position will be  (0,0,N).


```curl --request POST localhost:8080/rest/mars/MMRMMRMMRMM```

```curl --request POST localhost:8080/rest/mars/reset```

## Tests

```mvn test```

## Build

```mvn package```