package edipo.robot.core;

import java.util.HashMap;

public class MapDirections {
	
	private static final HashMap<Character, Direction> directionsMapLeft;
	static {
		directionsMapLeft = new HashMap<Character, Direction>();
		
		directionsMapLeft.put('N', Direction.WEST);
		directionsMapLeft.put('S', Direction.EAST);
		directionsMapLeft.put('E', Direction.NORTH);
		directionsMapLeft.put('W', Direction.SOUTH);
	}
	
	private static final HashMap<Character, Direction> directionsMapRight;
	static {
		directionsMapRight = new HashMap<Character, Direction>();
		
		directionsMapRight.put('N', Direction.EAST);
		directionsMapRight.put('S', Direction.WEST);
		directionsMapRight.put('E', Direction.SOUTH);
		directionsMapRight.put('W', Direction.NORTH);
	}
	
	public static HashMap<Character, Direction> directionsLeft(){
		return MapDirections.directionsMapLeft;
	}
	
	public static HashMap<Character, Direction> directionsRight(){
		return MapDirections.directionsMapRight;
	}

}
