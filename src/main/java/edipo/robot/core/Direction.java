package edipo.robot.core;

public enum Direction {

	NORTH(0, 'N'), EAST(1, 'E'), SOUTH(2, 'S'), WEST(3, 'W');

	private int direction;
	private char name;

	private Direction(int directionValue, char directionName) {
		direction = directionValue;
		name = directionName;
	}
	
	public int getDirection() { return direction; }
	public char getName() { return name; }
	
}
