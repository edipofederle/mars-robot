package edipo.robot.core;

public class Position {

	private int x = 0;
	private int y = 0;
	private Direction direction = null;
	private static final int SPACE_LIMIT = 25; // 5x5

	public Position(int x, int y, Direction direction) {
		this.x = x;
		this.y = y;
		this.direction = direction;
	}

	public String toString() {
		return this.x + "," + this.y + "," + this.direction.getName();
	}

	public void turnLeft() {
		direction = MapDirections.directionsLeft().get(direction.getName());
	}

	public void turnRight() {
		direction = MapDirections.directionsRight().get(direction.getName());
	}

	public Direction getDirection() {
		return this.direction;
	}

	private void incremmentX() {
		if (this.x + 1 <= SPACE_LIMIT)
			this.x++;
	}

	private void decrementX() {
		if (this.x - 1 >= 0)
			this.x--;
	}

	private void decrementY() {
		if (this.y - 1 >= 0)
			this.y--;
	}

	private void incremmentY() {
		if (this.y + 1 <= SPACE_LIMIT)
			this.y++;
	}

	public void move() {
		switch (Character.toLowerCase(this.direction.getName())) {
		case 'n':
			this.incremmentY();
			break;
		case 's':
			this.decrementY();
			break;
		case 'e':
			this.incremmentX();
			break;
		case 'w':
			this.decrementX();
			break;
		}
	}

	public int getY() {
		return this.y;
	}

	public int getX() {
		return this.x;
	}

	public void setDirection(Direction direction) {
		this.direction = direction;
	}

	public void setY(int y) {
		this.y = y;
	}

	public void setX(int x) {
		this.x = x;
	}

}
