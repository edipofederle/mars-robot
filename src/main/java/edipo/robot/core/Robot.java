package edipo.robot.core;

public class Robot {

	private Position position;

	public Robot() {
		this.position = new Position(0, 0, Direction.NORTH);
	}

	public String getPositionAsString() {
		return this.position.toString();
	}
	
	public Position getPosition() {
		return this.position;
	}

	public String move(String movementInstructions) throws Exception {
		for (char instruction : movementInstructions.toCharArray())
			handleInstruction(Character.toLowerCase(instruction));
		
		return this.getPositionAsString();
	}

	private void handleInstruction(char instruction) throws Exception {
		switch (instruction) {
		case 'l':
			this.position.turnLeft();
			break;
		case 'r':
			this.position.turnRight();
			break;
		case 'm':
			this.position.move();
			break;
		default:
			throw new Exception("Instruction: "
					+ Character.toUpperCase(instruction)
					+ " is invalid. Valid ones are: \"R\" \"L\" and \"M\"");
		}
	}

	public void restorePosition(Position currentPosition) {
		
		this.position.setX(currentPosition.getX());
		this.position.setY(currentPosition.getY());
		this.position.setDirection(currentPosition.getDirection());
	}
	
}
