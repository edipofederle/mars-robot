package edipo.robot.services;

import javax.annotation.PostConstruct;
import javax.json.Json;
import javax.json.JsonObject;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

import com.sun.jersey.spi.resource.Singleton;

import edipo.robot.core.Direction;
import edipo.robot.core.Position;
import edipo.robot.core.Robot;

@Singleton
@Path("/mars")
public class RobotService {

	private Robot robot = null;

	@PostConstruct
	public void init() {
		this.robot = new Robot();
	}

	@POST
	@Path("/{param}")
	public Response instructionRobot(@PathParam("param") String instructions) {
		JsonObject jsonResponse = null;
		Position currentPosition = robot.getPosition();

		try {
			String position = this.robot.move(instructions);
			jsonResponse = Json.createObjectBuilder().add("position", position).build();
		} catch (Exception e) {
			robot.restorePosition(currentPosition);
			jsonResponse = Json.createObjectBuilder().add("error", e.getMessage().toString()).build();
		}

		return Response.status(200).entity(jsonResponse.toString()).build();
	}
	
	@POST
	@Path("/reset")
	public Response resetRobot(){
		JsonObject jsonResponse = null;
		robot.restorePosition(new Position(0, 0, Direction.NORTH));
		jsonResponse = Json.createObjectBuilder().add("position", "Robot reseted. Position is now: " + robot.getPositionAsString()).build();
		
		return Response.status(200).entity(jsonResponse.toString()).build();
	}
}