package edipo.robot.core;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import edipo.robot.core.Robot;

@RunWith(Parameterized.class)
public class RobotParametrizedTest {
	
	private String robotInput;
	private String robotExpected;
	
	public RobotParametrizedTest(String input, String expected){
		robotInput = input;
		robotExpected = expected;
	}
	
	@Test
	public void test() throws Exception{
		Robot robot = new Robot();
		assertEquals(robotExpected, robot.move(robotInput));
	}
	
	@Parameters
	public static Collection<Object[]> data(){
		return Arrays.asList(new Object[][] {
				{"MML", "0,2,W"},
				{"MMRMMRMMRMM", "0,0,W"},
				{"RMLMMM", "1,3,N"},
				{"LMRMMM", "0,3,N"},
				{"MMMMMMMMMMMMMMMMMMMMMMMMMML","0,25,W"},
				{"LLMM","0,0,S"}
		});
	}

}
