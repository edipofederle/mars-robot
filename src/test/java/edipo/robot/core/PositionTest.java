package edipo.robot.core;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import edipo.robot.core.Direction;
import edipo.robot.core.Position;

public class PositionTest {
	
	@Test
	public void fromNorthWhenTurnLeftPositionShouldBeWest(){
		Position position = new Position(0, 0, Direction.NORTH);
		position.turnLeft();
		
		assertEquals('W', position.getDirection().getName());
	}
	
	@Test
	public void fromSouthWhenTurnLeftPositionShouldBeEast(){
		Position position = new Position(0, 0, Direction.SOUTH);
		position.turnLeft();
		
		assertEquals('E', position.getDirection().getName());
	}
	
	@Test
	public void fromEastWhenTurnLeftPositionShouldBeWest(){
		Position position = new Position(0, 0, Direction.EAST);
		position.turnLeft();
		assertEquals('N', position.getDirection().getName());
	}
	
	@Test
	public void fromWestWhenTurnLeftPositionShouldBeWest(){
		Position position = new Position(0, 0, Direction.WEST);
		position.turnLeft();
		assertEquals('S', position.getDirection().getName());
	}
	
	@Test
	public void fromNorthWhenTurnRightPositionShouldBeEast(){
		Position position = new Position(0, 0, Direction.NORTH);
		position.turnRight();
		
		assertEquals('E', position.getDirection().getName());
	}
	
	@Test
	public void fromSouthWhenTurnRightPositionShouldBeWest(){
		Position position = new Position(0, 0, Direction.SOUTH);
		position.turnRight();
		
		assertEquals('W', position.getDirection().getName());
	}
	
	@Test
	public void fromEastWhenTurnRightPositionShouldBeSouth(){
		Position position = new Position(0, 0, Direction.EAST);
		position.turnRight();
		
		assertEquals('S', position.getDirection().getName());
	}
	
	@Test
	public void fromWestWhenTurnRightPositionShouldBeNorth(){
		Position position = new Position(0, 0, Direction.WEST);
		position.turnRight();
		
		assertEquals('N', position.getDirection().getName());
	}
	

}
