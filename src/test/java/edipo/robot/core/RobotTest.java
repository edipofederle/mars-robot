package edipo.robot.core;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import edipo.robot.core.Position;
import edipo.robot.core.Robot;

public class RobotTest {
	
	@Test
	public void shouldStartsAtZeroZeroNothPosition(){
		Robot robot = new Robot();
		assertEquals(robot.getPositionAsString().toString(), "0,0,N");
	}
	
	@Test
	public void shouldMoveRoverTwoInstructionSeparated() throws Exception{
		Robot robot = new Robot();	
		robot.move("MML");
		robot.move("MML");
		
		assertEquals("0,2,S", robot.getPositionAsString());
	}
	
	@Test
	public void shoudRestorePosition() throws Exception{
		Robot robot = new Robot();
		robot.move("MML");
		Position position = robot.getPosition();
		
		try{
			robot.move("JFJFKF");
		}catch(Exception exp) {}
		
		robot.restorePosition(position);
		
		assertEquals("0,2,W", robot.getPositionAsString());
	}
}


